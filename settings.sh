# disable brew auto update
export HOMEBREW_NO_AUTO_UPDATE=1

INSTALL_PCLOUD=true

apps=(
    "brave-browser" "iterm2" "arduino" "alfred" "keka"
    "cheatsheet" "transmission" "qlcolorcode" "qlstephen"
    "bettertouchtool" "balenaetcher" "hiddenbar"
    "visual-studio-code" "iina" "appcleaner" "forklift"
    "ultimaker-cura" "brooklyn" "sourcetree"
    "bitwarden" "mqtt-explorer" "autodesk-fusion360"
    "angry-ip-scanner" "postman" "discord" "the-unarchiver" "viscosity"
    "notion" "runjs" "vagrant" 
    "mactex" "atext" "nordvpn" "grammarly"
)

cli_tools=(
    "duck" "thefuck" "dockutil" "speedtest-cli" "wget"
    "doxygen" "cppcheck" "shellcheck" "boost" "svn"
    "autoconf" "cmake" "mas" "m-cli" "p7zip" "switchaudio-osx"
    "duti" "java" "svn" "youtube-dl" "ffmpeg" "cloc" "neofetch"
    "wakeonlan" "nmap" "bpytop" "k6" "exa" "watch" "ansible"
    "automake" "conan"
)

fonts=(
    "font-montserrat"
	"font-hack-nerd-font"
)

macapps=(
    "bettersnaptool" "serialtools" "GarageBand" "spark"
)

remov_dock_icons=()
